var baseURL = "http://localhost:8080/servlet-1.0-SNAPSHOT";

function populateCv(container) {
    $.getJSON(baseURL + "/api/cv", function (cv) {
        var article = $("<article></article>");
        $("#"+container).append(article);

        article.append(createHeader('<h2>','cv'));

        article.append($("<section></section>")
            .append(createInfoTable('Basic information', cv.info)));

        article.append($("<section></section>")
            .append(createEduTable('education', cv.education, 'school', 'subject', 'years', 'title')));

        article.append($("<section></section>")
            .append(createExpTable('experience', cv.workExperience, 'company', 'location', 'position', 'years')));

        article.append($("<section></section>")
            .append(createHeader('<h3>', 'it skills'))
            .append(createList(cv.itSkills)));

        article.append($("<section></section>")
            .append(createHeader('<h3>', 'language skills'))
            .append(createList(cv.languageSkills)));

        article.append($("<section></section>")
            .append(createHeader('<h3>', 'interests'))
            .append(createList(cv.interests)));
    });
}

function createHeader(name, text) {
    return $("<header></header>").append($(name).text(text));
}

function createList(skills) {
    var ul = $("<ul></ul>");

    for (var index in skills) {
        var skill = skills[index];
        ul.append($("<li></li>").text(skill));
    }
    return ul;
}

function createExpTable(name, workExperience, v1, v2, v3, v4) {
    return $("<table></table>").addClass("Table").append($("<th></th>")
        .text(name).attr("colspan", workExperience.length))
        .append(createExpRow(workExperience, v1)).append(createExpRow(workExperience, v2))
        .append(createExpRow(workExperience, v3)).append(createExpRow(workExperience, v4));
}

function createExpRow(workExperience, name) {
    var tr = $("<tr></tr>");

    for (var index in workExperience) {
        var experience = workExperience[index];
        tr.append($("<td></td>").text(experience[name]));
    }

    return tr;
}

function createEduTable(name, education, v1, v2, v3, v4) {
    return $("<table></table>").addClass("Table").append($("<th></th>").text(name).attr("colspan", education.length))
        .append(createEduRow(education, v1)).append(createEduRow(education, v2))
        .append(createEduRow(education, v3)).append(createEduRow(education, v4));
}

function createEduRow(education, name) {
    var tr = $("<tr></tr>");

    for (var index in education) {
        var educatio = education[index];
        tr.append($("<td></td>").text(educatio[name]));
    }

    return tr;
}

function createInfoTable(name, info) {
    return $("<table></table>").addClass("Table").append($("<th></th>").attr("colspan", 2).text(name))
        .append(createOneRow("Name:", info.name)).append(createOneRow("Surname:", info.surname))
        .append(createOneRow("Adress:", info.adress1)).append(createOneRow("", info.adress2));
}

function createOneRow(header, value) {
    return $("<tr></tr>").append($("<td></td>").text(header)).append($("<td></td>").text(value));
}

function addWorkExp(form) {
    workExperience = {
        "company": form.children("input[name = company]").val(),
        "location": form.children("input[name = location]").val(),
        "position": form.children("input[name = position]").val(),
        "years": form.children("input[name = years]").val()
    };
    $.ajax(
        {
            type: "POST",
            url: baseURL + "/api/cv",
            data: JSON.stringify(workExperience),
            headers: {"Content-Type": "application/json"}
        }
    );
}